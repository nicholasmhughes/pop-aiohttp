The changelog format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

This project uses [Semantic Versioning](https://semver.org/) - MAJOR.MINOR.PATCH

# Changelog

Pop-aiohttp 1.2.0 (2022-12-27)
==============================

Added
-----

- Add response kwarg handling to server (#5)


Pop-aiohttp 1.1.1 (2022-09-05)
==============================

Fixed
-----

- Small error handling and kwarg passing fixes


Pop-aiohttp 1.1.0 (2022-07-17)
==============================

Fixed
-----

- Add basic web server handling with hub function router (#1)


Pop-aiohttp 1.0.0 (2022-07-07)
==============================

Added
-----

- Initial version of pop-aiohttp with basic functionality from idem-aiohttp
