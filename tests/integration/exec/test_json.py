async def test_delete(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="DELETE").respond_with_json({"foo": "bar"})
    url = httpserver.url_for("/foobar")

    result = await hub.exec.request.json.delete(ctx, url)
    assert result["comment"], "OK"
    assert result["ret"] == {"foo": "bar"}
    assert result["status"], True


async def test_get(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="GET").respond_with_json({"foo": "bar"})
    url = httpserver.url_for("/foobar")

    result = await hub.exec.request.json.get(ctx, url)
    assert result["comment"], "OK"
    assert result["ret"] == {"foo": "bar"}
    assert result["status"], True


async def test_head(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="HEAD").respond_with_json({"foo": "bar"})
    url = httpserver.url_for("/foobar")

    result = await hub.exec.request.json.head(ctx, url)
    ret = result["headers"]
    assert "Content-Length" in ret
    assert "Content-Type" in ret
    assert "Date" in ret
    assert "Server" in ret


async def test_patch(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PATCH").respond_with_json({"foo": "bar"})
    url = httpserver.url_for("/foobar")

    result = await hub.exec.request.json.patch(ctx, url)
    assert result["comment"], "OK"
    assert result["ret"] == {"foo": "bar"}
    assert result["status"], True


async def test_post(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="POST").respond_with_json({"foo": "bar"})
    url = httpserver.url_for("/foobar")

    result = await hub.exec.request.json.post(ctx, url)
    assert result["comment"], "OK"
    assert result["ret"] == {"foo": "bar"}
    assert result["status"], True


async def test_put(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PUT").respond_with_json({"foo": "bar"})
    url = httpserver.url_for("/foobar")

    result = await hub.exec.request.json.put(ctx, url)
    assert result["comment"], "OK"
    assert result["ret"] == {"foo": "bar"}
    assert result["status"], True
