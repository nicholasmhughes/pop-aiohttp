from unittest import mock


async def test_init_empty_routes(hub, ctx, aiohttp_client):
    app = await hub.server.web.init()
    client = await aiohttp_client(app)
    resp = await client.get("/")
    assert resp.status == 404


async def test_init_basic_routes(hub, ctx, aiohttp_client):
    text = "Hello, world"
    data = {"msg": "Hello, world"}

    func = mock.MagicMock(
        side_effect=[
            hub.server.web.response_handler(text),
            hub.server.web.response_handler(data),
            hub.server.web.response_handler("Success"),
        ]
    )
    routes = [
        [func],
        ["/json", func],
        ["post", "/json", func],
    ]

    app = await hub.server.web.init(routes=routes)
    client = await aiohttp_client(app)

    # text get route
    resp = await client.get("/")
    assert resp.status == 200
    text = await resp.text()
    assert text == "Hello, world"

    # json get route
    resp = await client.get("/json")
    assert resp.status == 200
    text = await resp.text()
    assert text == '{"msg": "Hello, world"}'

    # json post route
    resp = await client.post("/json", data=data)
    assert resp.status == 200
    text = await resp.text()
    assert text == "Success"
