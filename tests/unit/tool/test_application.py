from unittest import mock


async def test_client(hub, ctx):
    mock_client = mock.MagicMock()
    connector = await hub.tool.request.application.connector(ctx, mock.MagicMock)
    cookie_jar = await hub.tool.request.application.cookie_jar(ctx, mock.MagicMock)

    await hub.tool.request.application.client(ctx, mock_client)

    mock_client.assert_called_once_with(
        auth=None,
        connector_owner=False,
        connector=connector,
        cookie_jar=cookie_jar,
        headers={},
    )


# Validate that client session is cached when called more than once with the same parameters
async def test_client_cache(hub, ctx):
    mock_client = mock.MagicMock()
    connector = await hub.tool.request.application.connector(ctx, mock.MagicMock)
    cookie_jar = await hub.tool.request.application.cookie_jar(ctx, mock.MagicMock)

    await hub.tool.request.application.client(ctx, mock_client)
    # Call again, client should be cached on the hub
    await hub.tool.request.application.client(ctx, mock_client)

    mock_client.assert_called_once_with(
        auth=None,
        connector_owner=False,
        connector=connector,
        cookie_jar=cookie_jar,
        headers={},
    )


# Validate that client session is regenerated if 'Authorization' header is set
async def test_client_cache_auth_header(hub, ctx):
    mock_client = mock.MagicMock()
    connector = await hub.tool.request.application.connector(ctx, mock.MagicMock)
    cookie_jar = await hub.tool.request.application.cookie_jar(ctx, mock.MagicMock)

    await hub.tool.request.application.client(ctx, mock_client)

    # headers can contain Authorization token, client should be regenerated if it changes
    ctx.acct["headers"] = {"Authorization": "Bearer ..."}
    # Call again, client should be cached on the hub
    await hub.tool.request.application.client(ctx, mock_client)

    calls = [
        mock.call(
            auth=None,
            connector_owner=False,
            connector=connector,
            cookie_jar=cookie_jar,
            headers={},
        ),
        mock.call(
            auth=None,
            connector_owner=False,
            connector=connector,
            cookie_jar=cookie_jar,
            headers={"Authorization": "Bearer ..."},
        ),
    ]

    mock_client.assert_has_calls(calls)

    ctx.acct.clear()


async def test_resolver(hub, ctx):
    mock_resolve = mock.MagicMock()
    ret = await hub.tool.request.application.resolver(ctx, mock_resolve)
    assert ret

    mock_resolve.assert_called_once_with()


async def test_connector(hub, ctx):
    mock_connector = mock.MagicMock()
    resolver = await hub.tool.request.application.resolver(ctx, mock.MagicMock)
    ret = await hub.tool.request.application.connector(ctx, mock_connector)
    assert ret

    mock_connector.assert_called_once_with(resolver=resolver)
