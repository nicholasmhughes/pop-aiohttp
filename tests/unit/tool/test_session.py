async def test_delete(hub):
    # Verify that the func alias made it onto the hub
    assert hasattr(hub.tool.request.session, "delete")


async def test_get(hub, ctx, httpserver):
    # Verify that the func alias made it onto the hub
    assert hasattr(hub.tool.request.session, "get")


async def test_head(hub):
    # Verify that the func alias made it onto the hub
    assert hasattr(hub.tool.request.session, "get")


async def test_patch(hub):
    # Verify that the func alias made it onto the hub
    assert hasattr(hub.tool.request.session, "patch")


async def test_post(hub):
    # Verify that the func alias made it onto the hub
    assert hasattr(hub.tool.request.session, "post")


async def test_put(hub):
    # Verify that the func alias made it onto the hub
    assert hasattr(hub.tool.request.session, "put")


async def test_request_delete(hub):
    ...


async def test_request_get(hub):
    ...


async def test_request_head(hub):
    ...


async def test_request_patch(hub):
    ...


async def test_request_post(hub):
    ...


async def test_request_put(hub):
    ...
